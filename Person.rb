class Person
	attr_accessor :cpf, :name, :age, :birthday, :address

	def initialize cpf="", name="", age=20, birthday="", address=""
		@cpf = cpf
		@name = name
		@age = age
		@birthday = birthday
		@address = address
	end

	def presentation
		yield("CPF: #{@cpf}", "Nome: #{@name}", "Idade: #{@age}", "Nascimento: #{@birthday}", "Endereço: #{@address}")
	end

	def humor dia
		{
			segunda: "Triste", terca: "Desesperado", quarta: "Depressivo", quinta: "Abalado", sexta: "Atordoado", sabado: "Desolado", domingo: "Perplexo" 
		}[dia]
	end

	def atividade
		"#{@name} está programando e chorando"
	end

end
