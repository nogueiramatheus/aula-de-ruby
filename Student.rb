require_relative "Person.rb"

class Student < Person 
	attr_accessor :registration
	def initialize cpf="", name="", age=0, birthday="", address="", registration="", studing=false
        super cpf, name, age, birthday, address
		@registration = registration
		@studing = studing
	end

	def studing?
		return @studing
	end

	def move
		@studing = !@studing
	end

	def presentation
		yield("CPF: #{cpf}", "Nome: #{name}", "Idade: #{@age}", "Dt. Nasc.: #{@birthday}", "Endereço: #{@address}", "Matrícula: #{@registration}", "Está na escola: #{@studing}")
	end

	def activity
		"#{@name} está estudando"
	end
end

    