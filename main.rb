require_relative "Student.rb"
require_relative "Teacher.rb"
require_relative "Person.rb"


puts "Estudante:"

puts puts
student = Student.new "25685212814","Matheus Nogueira",20,"01/09/98","uma rua ai, 85, São Cristóvão", "323232", true

student.presentation do |cpf, name, age, birthday, address, registration, studing| 
    puts cpf, name, age, birthday, address, registration, studing
end

student.move
puts "Estudando? #{student.studing?}"
puts "Humor em #{:sexta}: #{student.humor :sexta}"
puts student.atividade

puts puts puts

puts "Professor:"

puts puts
teacher = Teacher.new "25685212814","Matheus Nogueira",20,"01/09/98","uma rua ai, 85, São Cristóvão", ['Computação', 'Arte da sofrência'], ['Fotografia', 'Sofrência']

teacher.presentation do |cpf, name, age, birthday, address, formation, matters|
    puts cpf, name, age, birthday, address, formation, matters
end

puts "Humor em #{:quarta}: #{teacher.humor :quarta}"
puts teacher.atividade

