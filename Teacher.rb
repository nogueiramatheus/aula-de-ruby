require_relative "Person.rb"

class Teacher < Person
	
	attr_accessor :formation, :matters

	def initialize cpf="", name="", age=0, birthday="", address="", formation=[], matters=[]
		super cpf, name, age, birthday, address
		@formation = formation 
		@matters = matters
	end

	def presentation
		yield(
			"CPF: #{@cpf}", "Nome: #{@name}", "Idade: #{@age}", "Nascimento: #{@birthday}", "Endereço: #{@address}", "Formação Acadêmica: #{@formation.join(', ')}",  "Matérias: #{@matters.join(', ')}"
		)
	end

end